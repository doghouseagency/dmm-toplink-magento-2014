# Top Links

Helper classes that generate URL's for use in layout xml - mostly to remove `top.links` links.

To remove the checkout button:

    <reference name="top.links">
        <action method="removeLinkByUrl"><url helper="doghouse_toplinks/getCheckoutUrl" /></action>
    </reference>