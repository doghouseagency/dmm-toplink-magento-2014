<?php

class Doghouse_Toplinks_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getCheckoutUrl() {
        return $this->_getUrl('checkout', array('_secure' => true));
    }

    public function getCartUrl() {
	   	return $this->_getUrl('checkout/cart');
    }

    public function getContactsUrl() {
    	return $this->_getUrl('contacts');
    }

}
